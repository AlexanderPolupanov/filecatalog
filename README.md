**Symfony file catalog project**

Displays catalog structure, allows navigation between catalogs, displays files inside selected catalog and their size. Project also has a console command to synchronize the database based on selected catalog in the file system.

---

## Install

Quick installation guide

1. Enter your projects directory and run `git clone https://AlexanderPolupanov@bitbucket.org/AlexanderPolupanov/filecatalog.git FileCatalog`
2. Go to the new project directory and run `composer install`
3. Copy .env.dist to .env (`cp .env.dist .env`)
4. Create mysql database and modify .env file adding database connection string
5. Run migrations (`php bin/console doctrine:migrations:migrate`)
6. Run console command to sync database with the file system (`php bin/console app:sync {path}`) where {path} parameter is the directory you want to sync
7. Run the server (`php bin/console server:start`)
8. Navigate to the url provided in the last command's output

---

## Notes

In the real world scenario I would use other method to load data from the database - load only once; use caching. Also I would add some tests.
