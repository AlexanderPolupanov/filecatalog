<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ConfirmationQuestion;

use App\Entity\Catalog;
use App\Entity\File;

class SyncCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:sync';


    protected function configure()
    {
        $this
            ->setDescription('Synchronizes database into current state of selected directory')
            ->addArgument('dir', InputArgument::REQUIRED, 'Filesystem directory to use for database sync')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $dir = $input->getArgument('dir');

        if (!file_exists($dir))
        {
            $io->error(sprintf('Directory doesn\'t exist: %s', $dir));
            return;
        }
        
        if ($dir == DIRECTORY_SEPARATOR) // Of course it can be hacked by using relative path
        {
            $io->error(sprintf('Sorry, that is too much!', $dir));
            return;
        }
        
        $io->title('You are about to sync database using filesystem catalog structure!');
        $q = new ConfirmationQuestion('This command may clear the database. Continue?', false);
        if (!$io->askQuestion($q)) 
        {
            $io->note('Cancelled by the user.');
            return;
        }

        try
        {
            $this->sync($dir, $io);
        }
        catch (\Exception $e)
        {
            $io->error($e->getMessage());
            return;
        }

        $io->success('Success!');
    }

    /**
     * Synchronization command
     *
     * @param $path string Directory to sync the database with
     * @param SymfonyStyle $io
     *
     * @throws \Exception
     */
    private function sync($path, SymfonyStyle $io)
    {
        $path = rtrim($path, DIRECTORY_SEPARATOR); // Filesystem directory
        $basename = basename($path);
        if ($basename == '')
        {
            $io->error(sprintf('Directory name cannot be empty! (%s)', $path));
            throw new \Exception('Directory name is empty!');
        }

        $rootCatalog = $this->getRootCatalog();
        // Check if root directory name is different
        if ($rootCatalog->getName() != $basename)
        {
            $io->warning('Root directory is different, copying structure from scratch!');
            $this->clearDatabase();
            $this->addCatalogWithContents($path, null, $io);
            return;
        }

        // Go through catalogs and add/update/delete
        $this->syncCatalog($path, $rootCatalog, $io);
    }

    /**
     * Synchronize catalogs recursively
     *
     * @param $path
     * @param Catalog $catalog
     * @param SymfonyStyle $io
     *
     * @throws \Exception
     */
    private function syncCatalog($path, Catalog $catalog, SymfonyStyle $io)
    {
        // Get list of filesystem directories
        $dirs = $this->getDirectories($path);

        // Go through database subcatalogs and remove obsolete records
        $dirs = $this->syncSubCatalogs($path, $catalog, $dirs, $io);

        // Go through new directories in the filesystem and add them to the database
        foreach ($dirs as $name => $val)
        {
            // Add directory with its contents to the database
            $this->addCatalogWithContents($path . DIRECTORY_SEPARATOR . $name, $catalog->getId(), $io);
        }

        // Get list of filesystem files
        $filenames = $this->getFilenames($path);

        // Go through database files and remove obsolete records / update file sizes
        $filenames = $this->updateAndRemoveFiles($path, $catalog, $filenames, $io);

        // Add new files to the database
        $this->addNewFiles($path, $catalog->getId(), $filenames, $io);
    }

    /**
     * Sync subcatalogs of specific catalog
     *
     * @param $path
     * @param Catalog $catalog
     * @param $dirs
     * @param SymfonyStyle $io
     *
     * @return mixed List of new subdirectories in the filesystem
     * @throws \Exception
     */
    private function syncSubCatalogs($path, Catalog $catalog, $dirs, SymfonyStyle $io)
    {
        foreach ($catalog->getCatalogs() as $subCatalog)
        {
            $name = $subCatalog->getName();
            // Check if filesystem directory does not exist
            if (empty($dirs[$name]))
            {
                // Remove record from the database
                $io->note('Removing catalog: ' . $name);
                $this->removeCatalog($subCatalog);
                continue;
            }
            // Recurse into subcatalogs
            $this->syncCatalog($path . DIRECTORY_SEPARATOR . $name, $subCatalog, $io);
            // Remove filesystem directory from the list (so that only new ones will remain)
            unset($dirs[$name]);
        }
        return $dirs;
    }

    /**
     * Go through database files and remove obsolete records / update file sizes based on filesystem files
     *
     * @param $path
     * @param Catalog $catalog
     * @param mixed $filenames List of filenames in the filesystem
     * @param SymfonyStyle $io
     * @return mixed Remaining list of files which are new in the filesystem
     */
    private function updateAndRemoveFiles($path, Catalog $catalog, $filenames, SymfonyStyle $io)
    {
        foreach ($catalog->getFiles() as $file)
        {
            $name = $file->getName();
            // Check if filesystem file does not exist
            if (empty($filenames[$name]))
            {
                // Remove record from the database
                $io->note('Removing file: ' . $name);
                $this->removeFile($file);
                continue;
            }
            else
            {
                // File exists, check if size changed
                $nameWithPath = $path . DIRECTORY_SEPARATOR . $name;
                $size = filesize($nameWithPath);
                if ($file->getSize() != $size)
                {
                    // Update file size in the database
                    $io->note('Updating file "' . $name . '" size from ' . $file->getSize() . ' to ' . $size);
                    $this->updateFileSize($file, $size);
                }
            }
            // Remove filesystem file from the list (so that only new ones will remain)
            unset($filenames[$name]);
        }
        return $filenames;
    }

    /**
     * Go through new files in the filesystem and add them to the database
     *
     * @param $path
     * @param int $catalogId
     * @param array $filenames Filenames of new files
     * @param SymfonyStyle $io
     */
    private function addNewFiles($path, $catalogId, $filenames, SymfonyStyle $io)
    {
        foreach ($filenames as $name => $val)
        {
            // Add file to the database
            $nameWithPath = $path . DIRECTORY_SEPARATOR . $name;
            $fSize = filesize($nameWithPath);
            $fExt = pathinfo($nameWithPath, PATHINFO_EXTENSION);
            $io->note('Adding file: ' . $name . ' size: ' . $fSize . ' type: ' . $fExt);
            $this->addFile($name, $fExt, $fSize, $catalogId);
        }
    }

    /**
     * Get directory list for specific path
     *
     * @param $path
     * @return array Directory names as array keys
     */
    private function getDirectories($path)
    {
        $dirs = [];
        foreach (glob($path . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR) as $path)
        {
            $dirs[basename($path)] = true;
        }
        return $dirs;
    }

    /**
     * Get filename list for specific path
     *
     * @param $path
     * @return array File names as array keys
     */
    private function getFilenames($path)
    {
        $names = [];
        foreach (glob($path . DIRECTORY_SEPARATOR . '*') as $path)
        {
            // Skip directories
            if (is_dir($path)) continue;

            $names[basename($path)] = true;
        }
        return $names;
    }

    /**
     * Clear database
     * Warning: You should add "ON DELETE CASCADE" manually to the migrations file (if it is not there)
     */
    private function clearDatabase()
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $doctrine->getRepository(Catalog::class)->removeAll();
    }

    /**
     * Add catalog to the database
     *
     * @param $name
     * @param int|null $parentId
     * @return int|null
     */
    private function addCatalog($name, $parentId = null)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        
        $parent = null;
        if ($parentId != null)
        {
            $parent = $doctrine->getRepository(Catalog::class)->find($parentId);
        }
        
        $cat = new Catalog();
        $cat->setName($name);
        $cat->setParent($parent);
        $em->persist($cat);
        
        $em->flush();
        
        return $cat->getId();
    }

    /**
     * Add file to the database
     *
     * @param $name
     * @param $type
     * @param $size
     * @param $catalogId
     * @return int|null
     */
    private function addFile($name, $type, $size, $catalogId)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        
        $cat = $doctrine->getRepository(Catalog::class)->find($catalogId);
        
        $file = new File();
        $file->setName($name);
        $file->setType($type);
        $file->setSize($size);
        $file->setCatalog($cat);
        $em->persist($file);
        
        $em->flush();
        
        return $file->getId();
    }

    /**
     * Alter file size in the database
     *
     * @param File $file
     * @param $size
     */
    private function updateFileSize(File $file, $size)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $file->setSize($size);
        $em->persist($file);

        $em->flush();
    }

    /**
     * Remove file from the database
     *
     * @param File $file
     */
    private function removeFile(File $file)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $em->remove($file);

        $em->flush();
    }

    /**
     * Get root catalog
     *
     * @return Catalog|null
     */
    private function getRootCatalog()
    {
        $doctrine = $this->getContainer()->get('doctrine');

        return $doctrine->getRepository(Catalog::class)->findOneBy(['Parent' => null]);
    }

    /**
     * Remove catalog from the database
     *
     * @param Catalog $catalog
     */
    private function removeCatalog(Catalog $catalog)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $em->remove($catalog);

        $em->flush();
    }

    /**
     * Recursively add directories and files to the database
     *
     * @param $dir string Directory path to start with
     * @param $baseCatalogId int|null Parent catalog ID
     * @param SymfonyStyle $io
     *
     * @throws \Exception
     */
    private function addCatalogWithContents($dir, $baseCatalogId, SymfonyStyle $io)
    {
        $dir = rtrim($dir, DIRECTORY_SEPARATOR);
        $items = glob($dir . DIRECTORY_SEPARATOR . '*');
        $base = basename($dir);

        if ($base == '')
        {
            $io->error(sprintf('Directory name cannot be empty! (%s)', $dir));
            throw new \Exception('Directory name is empty!');
        }

        // Add directory
        $io->note('Adding directory: ' . $base);
        $newCatalogId = $this->addCatalog($base, $baseCatalogId);
        if ($newCatalogId == null)
        {
            $io->error('addCatalog() returned null!');
            throw new \Exception('Cannot add catalog!');
        }

        foreach ($items as $item)
        {
            if (is_dir($item))
            {
                // Recursively add catalog with contents
                $this->addCatalogWithContents($item, $newCatalogId, $io);
            }
            else
            {
                // It is a file
                $fName = basename($item);
                $fSize = filesize($item);
                $fExt  = pathinfo($fName, PATHINFO_EXTENSION);
                $io->note('Adding file: ' . $fName . ' size: ' . $fSize . ' type: ' . $fExt);
                $this->addFile($fName, $fExt, $fSize, $newCatalogId);
            }
        }
    }
}
