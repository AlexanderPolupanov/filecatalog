<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Catalog;
use App\Entity\File;

class FileCatalogController extends AbstractController
{
    /**
     * @Route("/{catalogId<\d+>}", defaults={"catalogId"=0}, name="main")
     */
    public function index($catalogId)
    {
        $root = $this->getDoctrine()->getRepository(Catalog::class)->findOneBy(['Parent' => null]);
        
        $selected = null;
        if ($catalogId != 0)
        {
            $selected = $this->getDoctrine()->getRepository(Catalog::class)->find($catalogId);
        }
        if ($selected == null)
        {
            $selected = $root;
            $catalogId = $selected->getId();
        }
        
        $totalFiles = $this->getDoctrine()->getRepository(File::class)->getFilesCount();
        $totalSize = $this->getDoctrine()->getRepository(File::class)->getFilesSize();
        
        $data = [
            'root' => $root, 
            'selectedId' => $catalogId, 
            'selected' => $selected,
            'totalFiles' => $totalFiles,
            'totalSize' => $totalSize
        ];
        return $this->render('index.html.twig', $data);
    }

    /**
     * @Route("/ajax/{catalogId<\d+>}", name="ajax")
     */
    public function ajaxAction(Request $request, $catalogId)
    {
        $selected = $this->getDoctrine()->getRepository(Catalog::class)->find($catalogId);

        return new JsonResponse(['catalog' => [
            'id' => $selected->getId(),
            'name' => $selected->getName(),
            'files' => $this->getCatalogFilesAsArray($selected)
            ]]);
    }

    /**
     * Get catalog files as array
     *
     * @param Catalog $catalog
     * @return array
     */
    private function getCatalogFilesAsArray(Catalog $catalog)
    {
        $res = [];
        foreach ($catalog->getFiles() as $file)
        {
            $res[] = [
                'id' => $file->getId(),
                'name' => $file->getName(),
                'type' => $file->getType(),
                'size' => $file->getSize()
            ];
        }

        return $res;
    }
    
    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/FileCatalogController.php',
        ]);
    }
}
